package main.wat.edu.pl.asystentmagazynu.controllers;

import main.wat.edu.pl.asystentmagazynu.DataFactory;
import main.wat.edu.pl.asystentmagazynu.models.Order;
import main.wat.edu.pl.asystentmagazynu.models.Product;
import main.wat.edu.pl.asystentmagazynu.services.OrderService;
import main.wat.edu.pl.asystentmagazynu.services.RouteCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static main.wat.edu.pl.asystentmagazynu.DataFactory.ORDER_PATH;

@RestController
@CrossOrigin(origins = DataFactory.ORIGIN)
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(ORDER_PATH + "/{id}")
    @ResponseBody
    public Optional<Order> getOrder(@PathVariable(value = "id") Long id) {
        return orderService.getOrder(id);
    }

    @GetMapping(ORDER_PATH + "/all")
    public Iterable<Order> getOrders() {
        return orderService.getOrders();
    }

    @PostMapping(ORDER_PATH + "/{userId}")
    @ResponseBody
    public Order addOrder(@PathVariable(value = "userId") Long id, @RequestBody List<Product> products) {
        return orderService.addOrder(id, products);
    }

    @GetMapping(ORDER_PATH + "/route/{orderId}")
    @ResponseBody
    public Map<String, RouteCreator.Node> getRoute(@PathVariable(value = "orderId") Long orderId) {
        return orderService.getRoute(orderId);
    }
}
