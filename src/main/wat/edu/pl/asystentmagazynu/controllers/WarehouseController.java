package main.wat.edu.pl.asystentmagazynu.controllers;

import main.wat.edu.pl.asystentmagazynu.DataFactory;
import main.wat.edu.pl.asystentmagazynu.models.Warehouse;
import main.wat.edu.pl.asystentmagazynu.services.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static main.wat.edu.pl.asystentmagazynu.DataFactory.WAREHOUSE_PATH;

@RestController
@CrossOrigin(origins = DataFactory.ORIGIN)
public class WarehouseController {


    private final WarehouseService warehouseService;

    @Autowired
    public WarehouseController(WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    @GetMapping(WAREHOUSE_PATH + "/{id}")
    @ResponseBody
    public Optional<Warehouse> getWarehouse(@PathVariable(value = "id") Long id) {
        return warehouseService.getWarehouse(id);
    }

    @GetMapping(WAREHOUSE_PATH + "/all")
    public Iterable<Warehouse> getWarehouses() {
        return warehouseService.getWarehouses();
    }

    @PostMapping(WAREHOUSE_PATH)
    @ResponseBody
    public Warehouse addWarehouse(@RequestBody Warehouse warehouse) {
        return warehouseService.addWarehouse(warehouse);
    }
}
