package main.wat.edu.pl.asystentmagazynu.controllers;

import main.wat.edu.pl.asystentmagazynu.DataFactory;
import main.wat.edu.pl.asystentmagazynu.models.Zone;
import main.wat.edu.pl.asystentmagazynu.services.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static main.wat.edu.pl.asystentmagazynu.DataFactory.ZONE_PATH;

@RestController
@CrossOrigin(origins = DataFactory.ORIGIN)
public class ZoneController {

    private final ZoneService zoneService;


    @Autowired
    public ZoneController(ZoneService zoneService) {
        this.zoneService = zoneService;
    }

    @GetMapping(ZONE_PATH + "/{id}")
    @ResponseBody
    public Optional<Zone> getZone(@PathVariable("id") Long id) {
        return zoneService.getZone(id);
    }

    @GetMapping(ZONE_PATH + "/all")
    public Iterable<Zone> getZones() {
        return zoneService.getZones();
    }

    @PostMapping(ZONE_PATH + "/{warehouseId}")
    @ResponseBody
    public Zone createZone(@PathVariable("warehouseId") Long warehouseId, @RequestBody Zone zone) {
        return zoneService.createZone(warehouseId, zone);
    }
}
