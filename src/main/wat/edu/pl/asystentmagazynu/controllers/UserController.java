package main.wat.edu.pl.asystentmagazynu.controllers;

import main.wat.edu.pl.asystentmagazynu.DataFactory;
import main.wat.edu.pl.asystentmagazynu.models.User;
import main.wat.edu.pl.asystentmagazynu.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static main.wat.edu.pl.asystentmagazynu.DataFactory.USER_PATH;

@RestController
@CrossOrigin(origins = DataFactory.ORIGIN)
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(USER_PATH + "/{id}")
    @ResponseBody
    public Optional<User> getUser(@PathVariable(value = "id") Long id) {
        return userService.getUser(id);
    }

    @GetMapping(USER_PATH + "/all")
    public Iterable<User> getUsers() {
        return userService.getUsers();
    }

    @PostMapping(USER_PATH + "/{zoneId}")
    @ResponseBody
    public User createUser(@PathVariable(value = "zoneId") Long zoneId, @RequestBody User user) {
        return userService.createUser(zoneId, user);
    }
}
