package main.wat.edu.pl.asystentmagazynu.controllers;

import main.wat.edu.pl.asystentmagazynu.DataFactory;
import main.wat.edu.pl.asystentmagazynu.models.Place;
import main.wat.edu.pl.asystentmagazynu.services.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.Optional;

import static main.wat.edu.pl.asystentmagazynu.DataFactory.PLACE_PATH;

@RestController
@CrossOrigin(origins = DataFactory.ORIGIN)
public class PlaceController {

    private final PlaceService placeService;

    @Autowired
    public PlaceController(PlaceService placeService) {
        this.placeService = placeService;
    }

    @GetMapping(PLACE_PATH + "/{id}")
    @ResponseBody
    public Optional<Place> getPlace(@PathVariable("id") Long id) {
        return placeService.getPlace(id);
    }

    @GetMapping(PLACE_PATH + "/all")
    public Iterable<Place> getPlaces() {
        return placeService.getPlaces();
    }

    @GetMapping(PLACE_PATH + "/all/{zoneId}")
    public LinkedList<Place> getPlacesByZone(@PathVariable(value = "zoneId") Long zoneId) {
        return placeService.getPlacesByZone(zoneId);
    }

    @PostMapping(PLACE_PATH + "/{zoneId}/{productId}")
    @ResponseBody
    public Place addPlace(@PathVariable(value = "zoneId") Long zoneId, @PathVariable(value = "productId") Long productId, @RequestBody Place place) {
        return placeService.addPlace(zoneId, productId, place);
    }
}
