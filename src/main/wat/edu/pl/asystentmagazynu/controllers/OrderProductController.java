package main.wat.edu.pl.asystentmagazynu.controllers;

import main.wat.edu.pl.asystentmagazynu.DataFactory;
import main.wat.edu.pl.asystentmagazynu.models.OrderProduct;
import main.wat.edu.pl.asystentmagazynu.services.OrderProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static main.wat.edu.pl.asystentmagazynu.DataFactory.ORDER_PRODUCT_PATH;

@RestController
@CrossOrigin(origins = DataFactory.ORIGIN)
public class OrderProductController {

    private final OrderProductService orderProductService;

    @Autowired
    public OrderProductController(OrderProductService orderProductService) {
        this.orderProductService = orderProductService;
    }

    @GetMapping(ORDER_PRODUCT_PATH + "/{id}")
    @ResponseBody
    public Optional<OrderProduct> getOrderProduct(@PathVariable(value = "id") Long id) {
        return orderProductService.getOrderProduct(id);
    }

    @GetMapping(ORDER_PRODUCT_PATH + "/all")
    public Iterable<OrderProduct> getOrderProducts() {
        return orderProductService.getOrderProducts();
    }

    @GetMapping(ORDER_PRODUCT_PATH + "/all/{orderId}")
    @ResponseBody
    public Iterable<OrderProduct> getOrderProductsByOrder(@PathVariable(value = "orderId") Long id) {
        return orderProductService.getOrderProductsByOrder(id);
    }

    @PostMapping(ORDER_PRODUCT_PATH + "/{orderId}/{productId}")
    @ResponseBody
    public OrderProduct addOrderProduct(@PathVariable(value = "orderId") Long orderId, @PathVariable(value = "productId") Long productId, @RequestBody OrderProduct orderProduct) {
        return orderProductService.addOrderProduct(orderId, productId, orderProduct);
    }
}
