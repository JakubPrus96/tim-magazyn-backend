package main.wat.edu.pl.asystentmagazynu.controllers;

import main.wat.edu.pl.asystentmagazynu.DataFactory;
import main.wat.edu.pl.asystentmagazynu.models.Product;
import main.wat.edu.pl.asystentmagazynu.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static main.wat.edu.pl.asystentmagazynu.DataFactory.PRODUCT_PATH;

@RestController
@CrossOrigin(origins = DataFactory.ORIGIN)
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(PRODUCT_PATH + "/{id}")
    @ResponseBody
    public Optional<Product> getProduct(@PathVariable(value = "id") Long id) {
        return productService.getProduct(id);
    }

    @GetMapping(PRODUCT_PATH + "/all")
    public Iterable<Product> getProducts() {
        return productService.getProducts();
    }

    @PutMapping(PRODUCT_PATH + "/{warehouseId}")
    @ResponseBody
    public Product addProduct(@RequestBody Product body, @PathVariable Integer warehouseId) {
        return productService.addProduct(body, warehouseId);
    }
}
