package main.wat.edu.pl.asystentmagazynu.services;

import main.wat.edu.pl.asystentmagazynu.DataFactory;
import main.wat.edu.pl.asystentmagazynu.models.Place;
import main.wat.edu.pl.asystentmagazynu.models.Product;
import main.wat.edu.pl.asystentmagazynu.models.Warehouse;
import main.wat.edu.pl.asystentmagazynu.models.Zone;
import main.wat.edu.pl.asystentmagazynu.repositories.PlaceRepository;
import main.wat.edu.pl.asystentmagazynu.repositories.WarehouseRepository;
import main.wat.edu.pl.asystentmagazynu.repositories.ZoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class ProductPlacer {

    private final WarehouseRepository warehouseRepository;
    private final ZoneRepository zoneRepository;
    private final PlaceRepository placeRepository;
    private LinkedList<Place> places;
    private Place productPlace;

    @Autowired
    public ProductPlacer(WarehouseRepository warehouseRepository, ZoneRepository zoneRepository, PlaceRepository placeRepository) {
        this.warehouseRepository = warehouseRepository;
        this.zoneRepository = zoneRepository;
        this.placeRepository = placeRepository;
    }

    void findPlaceForProduct(Product product, Integer warehouseId) throws Exception {
        Optional<Warehouse> warehouse = warehouseRepository.findById(Long.valueOf(warehouseId));
        LinkedList<Zone> zones = zoneRepository.findByWarehouse(warehouse.get());
        productPlace = new Place();
        this.places = placeRepository.findByProduct(product);
        setZoneForPlace(zones, false);
        productPlace.setProduct(product);
        setPlaceSize(product);
        Boolean[][] slotsInZone = setBusyPlaces();
        while (!findFreeSlot(slotsInZone)) {
            slotsInZone = findAnotherSlot(zones);
        }
        placeRepository.save(productPlace);
    }

    private Boolean[][] findAnotherSlot(LinkedList<Zone> zones) throws Exception {
        Boolean[][] slotsInZone;
        zones.remove(productPlace.getZone());
        if (zones.size() == 0) {
            throw new Exception("Warehouse is full!!");
        }
        setZoneForPlace(zones, true);
        slotsInZone = setBusyPlaces();
        return slotsInZone;
    }

    private void setZoneForPlace(LinkedList<Zone> zones, Boolean isTryingToFindFreeSlot) {
        for (Zone zone : zones) {
            boolean isProductInZone = false;
            for (Place place : places) {
                isProductInZone = zone.id.equals(place.getZone().id);
                if (isProductInZone) {
                    break;
                }
            }
            if (!isProductInZone) {
                productPlace.setZone(zone);
                break;
            }
        }
        if (productPlace.getZone() == null || isTryingToFindFreeSlot) {
            productPlace.setZone(zones.get(ThreadLocalRandom.current().nextInt(0, zones.size())));
        }
    }

    private void setPlaceSize(Product product) {
        productPlace.setWidth(product.getWidth());
        productPlace.setLength(product.getLength());
    }

    private Boolean[][] createClearSlotsInZone(Place productPlace) {
        Boolean[][] slotsInZone = new Boolean[productPlace.getZone().getLength()][productPlace.getZone().getWidth()];
        for (int i = 0; i < productPlace.getZone().getLength(); i++) {
            for (int j = 0; j < productPlace.getZone().getWidth(); j++) {
                slotsInZone[i][j] = true;
            }
        }
        return slotsInZone;
    }

    private Boolean[][] setBusyPlaces() {
        LinkedList<Place> placesByZone = placeRepository.findByZone(productPlace.getZone());
        Boolean[][] slotsInZone = createClearSlotsInZone(productPlace);
        for (Place place : placesByZone) {
            addToBusyPlaces(slotsInZone, place);
        }
        return slotsInZone;
    }

    private Boolean findFreeSlot(Boolean[][] slotsInZone) {
        boolean isPlaceFounded = false;
        for (int j = 0; j < productPlace.getZone().getWidth() - productPlace.getWidth(); j += DataFactory.SHELF_WIDTH) {
            for (int i = 0; i < productPlace.getZone().getLength() - productPlace.getLength(); i++) {
                if (slotsInZone[i][j]) {
                    for (int length = 0; length < productPlace.getLength(); length++) {
                        if (slotsInZone[i + length][j]) {
                            isPlaceFounded = true;
                        } else {
                            break;
                        }
                    }
                    if (isPlaceFounded) {
                        productPlace.setCoordX(i);
                        productPlace.setCoordY(j);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void addToBusyPlaces(Boolean[][] slotsInZone, Place place) {
        for (int i = 0; i < place.getLength(); i++) {
            for (int j = 0; j < place.getWidth(); j++) {
                if (place.getCoordX() + i < slotsInZone.length && place.getCoordY() + j < slotsInZone.length)
                    slotsInZone[place.getCoordX() + i][place.getCoordY() + j] = false;
            }
        }
    }

}
