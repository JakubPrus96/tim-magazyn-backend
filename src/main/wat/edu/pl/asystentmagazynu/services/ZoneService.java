package main.wat.edu.pl.asystentmagazynu.services;

import main.wat.edu.pl.asystentmagazynu.models.Zone;
import main.wat.edu.pl.asystentmagazynu.repositories.WarehouseRepository;
import main.wat.edu.pl.asystentmagazynu.repositories.ZoneRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ZoneService {

    private final ZoneRepository zoneRepository;
    private final WarehouseRepository warehouseRepository;
    private final ZoneLineCreator zoneLineCreator;

    @Autowired
    public ZoneService(ZoneRepository zoneRepository, WarehouseRepository warehouseRepository, ZoneLineCreator zoneLineCreator) {
        this.zoneRepository = zoneRepository;
        this.warehouseRepository = warehouseRepository;
        this.zoneLineCreator = zoneLineCreator;
    }

    public Optional<Zone> getZone(Long id) {
        return zoneRepository.findById(id);
    }

    public Iterable<Zone> getZones() {
        return zoneRepository.findAll();
    }

    public Zone createZone(Long warehouseId, Zone zone) {
        return warehouseRepository.findById(warehouseId).map(warehouse -> {
            zone.setWarehouse(warehouse);
            zoneRepository.save(zone);
            zoneLineCreator.createLinesInZone(zone);
            return zone;
        }).orElseThrow(() -> new ResourceNotFoundException("Warehouse with Id " + warehouseId + " not found"));
    }
}
