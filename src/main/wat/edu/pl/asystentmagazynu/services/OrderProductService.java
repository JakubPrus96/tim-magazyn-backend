package main.wat.edu.pl.asystentmagazynu.services;

import main.wat.edu.pl.asystentmagazynu.models.Order;
import main.wat.edu.pl.asystentmagazynu.models.OrderProduct;
import main.wat.edu.pl.asystentmagazynu.models.Product;
import main.wat.edu.pl.asystentmagazynu.repositories.OrderProductRepository;
import main.wat.edu.pl.asystentmagazynu.repositories.OrderRepository;
import main.wat.edu.pl.asystentmagazynu.repositories.ProductRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Optional;

@Service
public class OrderProductService {

    private final OrderProductRepository orderProductRepository;
    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;

    @Autowired
    public OrderProductService(OrderProductRepository orderProductRepository, ProductRepository productRepository, OrderRepository orderRepository) {
        this.orderProductRepository = orderProductRepository;
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
    }

    public Optional<OrderProduct> getOrderProduct(Long id) {
        return orderProductRepository.findById(id);
    }

    public Iterable<OrderProduct> getOrderProducts() {
        return orderProductRepository.findAll();
    }

    public Iterable<OrderProduct> getOrderProductsByOrder(Long id) {
        return orderProductRepository.findByOrder_Id(id);
    }

    public OrderProduct addOrderProduct(Long orderId, Long productId, OrderProduct orderProduct) {
        return orderRepository.findById(orderId).map(order -> productRepository.findById(productId).map(product -> {
            orderProduct.setOrder(order);
            orderProduct.setProduct(product);
            return orderProductRepository.save(orderProduct);
        }).orElseThrow(() -> new ResourceNotFoundException("Product with Id " + productId + " not found"))).orElseThrow(() -> new ResourceNotFoundException("Order with Id " + orderId + " not found"));
    }

    void createOrderProducts(Order order, ArrayList<Product> products) {
        LinkedList<OrderProduct> newOrderProducts = new LinkedList<>();
        for (Product product : products) {
            OrderProduct newOrderProduct = new OrderProduct();
            newOrderProduct.setProduct(productRepository.findById(product.id).get());
            newOrderProduct.setOrder(order);
            newOrderProducts.add(newOrderProduct);
        }
        orderProductRepository.saveAll(newOrderProducts);
    }
}
