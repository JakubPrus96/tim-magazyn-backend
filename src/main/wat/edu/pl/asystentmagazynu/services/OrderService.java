package main.wat.edu.pl.asystentmagazynu.services;

import main.wat.edu.pl.asystentmagazynu.models.Order;
import main.wat.edu.pl.asystentmagazynu.models.Product;
import main.wat.edu.pl.asystentmagazynu.repositories.OrderRepository;
import main.wat.edu.pl.asystentmagazynu.repositories.UserRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final OrderProductService orderProductService;
    private final RouteCreator routeCreator;

    @Autowired
    public OrderService(OrderRepository orderRepository, UserRepository userRepository, OrderProductService orderProductService, RouteCreator routeCreator) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.orderProductService = orderProductService;
        this.routeCreator = routeCreator;
    }

    public Optional<Order> getOrder(Long id) {
        return orderRepository.findById(id);
    }

    public Iterable<Order> getOrders() {
        return orderRepository.findAll();
    }

    public Order addOrder(Long id, List<Product> products) {
        return userRepository.findById(id).map(user -> {
            Order order = new Order();
            order.setUser(user);
            orderProductService.createOrderProducts(order, (ArrayList<Product>) products);
            return orderRepository.save(order);
        }).orElseThrow(() -> new ResourceNotFoundException("User with Id " + id + " not found"));
    }

    public Map<String, RouteCreator.Node> getRoute(Long orderId) {
        return routeCreator.getCollectRoute(orderId);
    }
}
