package main.wat.edu.pl.asystentmagazynu.services;

import main.wat.edu.pl.asystentmagazynu.models.Place;
import main.wat.edu.pl.asystentmagazynu.repositories.PlaceRepository;
import main.wat.edu.pl.asystentmagazynu.repositories.ProductRepository;
import main.wat.edu.pl.asystentmagazynu.repositories.ZoneRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.Optional;

@Service
public class PlaceService {

    private final PlaceRepository placeRepository;
    private final ZoneRepository zoneRepository;
    private final ProductRepository productRepository;

    @Autowired
    public PlaceService(PlaceRepository placeRepository, ZoneRepository zoneRepository, ProductRepository productRepository) {
        this.placeRepository = placeRepository;
        this.zoneRepository = zoneRepository;
        this.productRepository = productRepository;
    }

    public Optional<Place> getPlace(Long id) {
        return placeRepository.findById(id);
    }

    public Iterable<Place> getPlaces() {
        return placeRepository.findAll();
    }

    public LinkedList<Place> getPlacesByZone(Long zoneId) {
        return placeRepository.findByZoneId(zoneId);
    }

    public Place addPlace(Long zoneId, Long productId, Place place) {
        return zoneRepository.findById(zoneId).map(zone -> productRepository.findById(productId).map(product -> {
            place.setZone(zone);
            place.setProduct(product);
            return placeRepository.save(place);
        }).orElseThrow(() -> new ResourceNotFoundException("Product with Id " + productId + " not found"))).orElseThrow(() -> new ResourceNotFoundException("Zone with Id " + zoneId + " not found"));
    }
}
