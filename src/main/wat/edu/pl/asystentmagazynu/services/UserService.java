package main.wat.edu.pl.asystentmagazynu.services;

import main.wat.edu.pl.asystentmagazynu.models.User;
import main.wat.edu.pl.asystentmagazynu.repositories.UserRepository;
import main.wat.edu.pl.asystentmagazynu.repositories.ZoneRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final ZoneRepository zoneRepository;

    @Autowired
    public UserService(UserRepository userRepository, ZoneRepository zoneRepository) {
        this.userRepository = userRepository;
        this.zoneRepository = zoneRepository;
    }

    public Optional<User> getUser(Long id) {
        return userRepository.findById(id);
    }

    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    public User createUser(Long zoneId, User user) {
        return zoneRepository.findById(zoneId).map(zone -> {
            user.setZone(zone);
            return userRepository.save(user);
        }).orElseThrow(() -> new ResourceNotFoundException("Zone with Id " + zoneId + " not found"));
    }
}
