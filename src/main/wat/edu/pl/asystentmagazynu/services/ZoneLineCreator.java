package main.wat.edu.pl.asystentmagazynu.services;

import main.wat.edu.pl.asystentmagazynu.DataFactory;
import main.wat.edu.pl.asystentmagazynu.models.Place;
import main.wat.edu.pl.asystentmagazynu.models.Zone;
import main.wat.edu.pl.asystentmagazynu.repositories.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

@Service
public class ZoneLineCreator {

    private final PlaceRepository placeRepository;

    @Autowired
    public ZoneLineCreator(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    void createLinesInZone(Zone zone) {
        LinkedList<Place> places = new LinkedList<>();
        places.add(createFirstAndLastLine(zone, 0));
        places.add(createFirstAndLastLine(zone, zone.getWidth() - DataFactory.ZONE_LANE_WIDTH / 2));
        for (int i = DataFactory.ZONE_LANE_WIDTH + DataFactory.ZONE_LANE_WIDTH / 2; i < zone.getWidth(); i += DataFactory.ZONE_LANE_DISTANCE) {
            Place place = new Place();
            place.setZone(zone);
            place.setWidth(DataFactory.ZONE_LANE_WIDTH);
            place.setLength(zone.getLength());
            place.setCoordX(0);
            place.setCoordY(i);
            places.add(place);
        }
        placeRepository.saveAll(places);
    }

    private Place createFirstAndLastLine(Zone zone, Integer coordY) {
        Place place = new Place();
        place.setZone(zone);
        place.setWidth(DataFactory.ZONE_LANE_WIDTH / 2);
        place.setLength(zone.getLength());
        place.setCoordX(0);
        place.setCoordY(coordY);
        return place;
    }
}
