package main.wat.edu.pl.asystentmagazynu.services;

import main.wat.edu.pl.asystentmagazynu.models.Product;
import main.wat.edu.pl.asystentmagazynu.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductPlacer productPlacer;

    @Autowired
    public ProductService(ProductRepository productRepository, ProductPlacer productPlacer) {
        this.productRepository = productRepository;
        this.productPlacer = productPlacer;
    }

    public Product addProduct(Product body, Integer warehouseId) {
        Product product;
        try {
            product = productRepository.findByName(body.name);
            product.quantity += body.quantity;
        } catch (Exception e) {
            product = body;
        }
        Product returnProduct = productRepository.save(product);
        try {
            for (int i = 0; i < body.quantity; i++) {
                productPlacer.findPlaceForProduct(returnProduct, warehouseId);
            }
        } catch (Exception e) {
            return new Product();
        }
        return returnProduct;
    }

    public Optional<Product> getProduct(Long id) {
        return productRepository.findById(id);
    }

    public Iterable<Product> getProducts() {
        return productRepository.findAll();
    }
}
