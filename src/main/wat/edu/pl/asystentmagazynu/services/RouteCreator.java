package main.wat.edu.pl.asystentmagazynu.services;

import main.wat.edu.pl.asystentmagazynu.DataFactory;
import main.wat.edu.pl.asystentmagazynu.models.*;
import main.wat.edu.pl.asystentmagazynu.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RouteCreator {

    private final OrderProductRepository orderProductRepository;
    private final OrderRepository orderRepository;
    private final PlaceRepository placeRepository;

    @Autowired
    public RouteCreator(OrderProductRepository orderProductRepository, OrderRepository orderRepository, PlaceRepository placeRepository) {
        this.orderProductRepository = orderProductRepository;
        this.orderRepository = orderRepository;
        this.placeRepository = placeRepository;
    }

    public Map<String, Node> getCollectRoute(Long orderId) {
        Order order = orderRepository.findById(orderId).get();
        User user = order.getUser();
        ArrayList<Product> products = getProductsFromOrder(order);
        LinkedList<Place> places = placeRepository.findAllByProductInAndZone(products, user.getZone());

        HashMap<Integer, HashMap<String, Node>> mapByProductOrder = new HashMap<>();

        createVariousRoutes(products, user, places, mapByProductOrder, user.getZone());
        return mapByProductOrder.get(getShortestRouteMap(mapByProductOrder));
    }

    private ArrayList<Product> getProductsFromOrder(Order order) {
        LinkedList<OrderProduct> orderProducts = orderProductRepository.findByOrder(order);
        ArrayList<Product> products = new ArrayList<>();
        for (OrderProduct orderProduct : orderProducts) {
            products.add(orderProduct.getProduct());
        }
        return products;
    }

    private void createVariousRoutes(ArrayList<Product> products, User user, LinkedList<Place> places, HashMap<Integer, HashMap<String, Node>> mapByProductOrder, Zone zone) {
        for (int i = 0; i < products.size(); i++) {
            LinkedList<Node> nodes = getFirstNodes(user, places);
            HashMap<String, Node> nodeByProductName = new HashMap<>();
            mapByProductOrder.put(i, nodeByProductName);
            for (Product product : products) {
                for (Node node : nodes) {
                    createNodesToCollectMap(nodeByProductName, product, node);
                }
                editNodesPrevious(nodes, nodeByProductName.get(product.name), nodeByProductName, zone);
            }
            Product[] productArray = new Product[products.size()];
            products = reorderArray(products.toArray(productArray));
        }
    }

    private LinkedList<Node> getFirstNodes(User user, LinkedList<Place> places) {
        LinkedList<Node> nodes = new LinkedList<>();
        Node startNode = new Node();
        startNode.coordX = 0;
        startNode.coordY = 0;
        createNodesFromPlaces(user, places, nodes, startNode);
        return nodes;
    }

    private void createNodesFromPlaces(User user, LinkedList<Place> places, LinkedList<Node> nodes, Node previousNode) {
        for (Place place : places) {
            Node node = new Node();
            node.place = place;
            node.coordY = place.getCoordY();
            node.coordX = place.getCoordX();
            node.previousCoordX = previousNode.coordX;
            node.previousCoordY = previousNode.coordY;
            node.productName = place.getProduct().getName();
            node.setDistanceToPrevious(place.getZone());
            nodes.add(node);
        }
    }

    private Integer getShortestRouteMap(HashMap<Integer, HashMap<String, Node>> mapByProductOrder) {
        Integer distance = 999999;
        Integer mapKey = 0;
        for (Integer i : mapByProductOrder.keySet()) {
            Integer totalDistance = 0;
            for (String productName : mapByProductOrder.get(i).keySet()) {
                totalDistance += mapByProductOrder.get(i).get(productName).distanceToPrevious;
            }
            if (totalDistance < distance) {
                distance = totalDistance;
                mapKey = i;
            }
        }
        return mapKey;
    }

    private void editNodesPrevious(LinkedList<Node> nodes, Node previousNode, HashMap<String, Node> nodesByProductName, Zone zone) {
        for (Node node : nodes) {
            if (!nodesByProductName.containsKey(node.productName)) {
                node.previousCoordY = previousNode.coordY;
                node.previousCoordX = previousNode.coordX;
                node.setDistanceToPrevious(zone);
            }
        }
    }

    private void createNodesToCollectMap(HashMap<String, Node> nodeByProductName, Product product, Node node) {
        if (node.productName.equals(product.name)) {
            if (nodeByProductName.containsKey(product.name)) {
                if (nodeByProductName.get(product.name).distanceToPrevious > node.distanceToPrevious) {
                    nodeByProductName.replace(product.name, node);
                }
            } else {
                nodeByProductName.put(product.name, node);
            }
        }
    }

    private ArrayList<Product> reorderArray(Product[] current) {
        Product[] reordered = new Product[current.length];
        for (int i = 0; i < current.length; i++) {
            if (i < current.length - 1) {
                reordered[i + 1] = current[i];
            } else {
                reordered[0] = current[i];
            }
        }
        return new ArrayList<>(Arrays.asList(reordered));
    }

    @Component
    public class Node {
        public String productName;
        public Integer coordX;
        public Integer coordY;
        public Integer previousCoordX;
        public Integer previousCoordY;
        public Integer distanceToPrevious;
        public Place place;

        Node() {
        }

        void setDistanceToPrevious(Zone zone) {
            this.distanceToPrevious = Math.abs(this.coordY - this.previousCoordY);
            if (Math.abs(this.coordY - this.previousCoordY) > DataFactory.SHELF_WIDTH) {
                if (this.coordX + this.previousCoordX > zone.getLength()) {
                    this.distanceToPrevious += Math.abs((zone.getLength() - this.previousCoordX) + (zone.getLength() - this.coordX));
                } else {
                    this.distanceToPrevious += this.coordX + this.previousCoordX;
                }
            } else {
                this.distanceToPrevious += Math.abs(this.coordX - this.previousCoordX);
            }
        }
    }
}
