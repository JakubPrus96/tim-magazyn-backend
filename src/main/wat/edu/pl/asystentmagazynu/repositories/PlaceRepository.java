package main.wat.edu.pl.asystentmagazynu.repositories;

import main.wat.edu.pl.asystentmagazynu.models.Place;
import main.wat.edu.pl.asystentmagazynu.models.Product;
import main.wat.edu.pl.asystentmagazynu.models.Zone;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Optional;

public interface PlaceRepository extends CrudRepository<Place, Long> {

    Optional<Place> findById(Long id);

    LinkedList<Place> findAll();

    LinkedList<Place> findByZone(Zone zone);

    LinkedList<Place> findByZoneId(Long zoneId);

    LinkedList<Place> findByProduct(Product product);

    LinkedList<Place> findAllByProductInAndZone(ArrayList<Product> products, Zone zone);

    @Transactional
    void deleteById(Long placeId);
}
