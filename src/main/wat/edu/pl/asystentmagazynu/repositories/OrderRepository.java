package main.wat.edu.pl.asystentmagazynu.repositories;

import main.wat.edu.pl.asystentmagazynu.models.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface OrderRepository extends CrudRepository<Order, Long> {

    Optional<Order> findById(Long id);
}
