package main.wat.edu.pl.asystentmagazynu.repositories;

import main.wat.edu.pl.asystentmagazynu.models.Warehouse;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface WarehouseRepository extends CrudRepository<Warehouse, Long> {

    Optional<Warehouse> findById(Long id);
}
