package main.wat.edu.pl.asystentmagazynu.repositories;

import main.wat.edu.pl.asystentmagazynu.models.Order;
import main.wat.edu.pl.asystentmagazynu.models.OrderProduct;
import org.springframework.data.repository.CrudRepository;

import java.util.LinkedList;
import java.util.Optional;

public interface OrderProductRepository extends CrudRepository<OrderProduct, Long> {

    Optional<OrderProduct> findById(Long id);

    Iterable<OrderProduct> findByOrder_Id(Long id);

    LinkedList<OrderProduct> findByOrder(Order order);
}
