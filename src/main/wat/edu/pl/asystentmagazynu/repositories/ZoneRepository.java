package main.wat.edu.pl.asystentmagazynu.repositories;

import main.wat.edu.pl.asystentmagazynu.models.Warehouse;
import main.wat.edu.pl.asystentmagazynu.models.Zone;
import org.springframework.data.repository.CrudRepository;

import java.util.LinkedList;
import java.util.Optional;

public interface ZoneRepository extends CrudRepository<Zone, Long> {

    Optional<Zone> findById(Long id);

    LinkedList<Zone> findByWarehouse(Warehouse warehouse);
}
