package main.wat.edu.pl.asystentmagazynu.repositories;

import main.wat.edu.pl.asystentmagazynu.models.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProductRepository extends CrudRepository<Product, Long> {

    Optional<Product> findById(Long id);

    Product findByName(String name);
}
