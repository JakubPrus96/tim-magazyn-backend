package main.wat.edu.pl.asystentmagazynu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsystentMagazynuApplication {

    public static void main(String[] args) {
        SpringApplication.run(AsystentMagazynuApplication.class, args);
    }
}
