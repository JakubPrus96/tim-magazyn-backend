package main.wat.edu.pl.asystentmagazynu;

public class DataFactory {

    public static final Integer SHELF_WIDTH = 50;
    public static final Integer ZONE_LANE_WIDTH = 2 * SHELF_WIDTH;
    public static final Integer ZONE_LANE_DISTANCE = 2 * ZONE_LANE_WIDTH;

    public static final String ORIGIN = "http://localhost:4200";
    public static final String ZONE_PATH = "/zone";
    public static final String WAREHOUSE_PATH = "/warehouse";
    public static final String USER_PATH = "/user";
    public static final String PRODUCT_PATH = "/product";
    public static final String PLACE_PATH = "/place";
    public static final String ORDER_PRODUCT_PATH = "/order-product";
    public static final String ORDER_PATH = "/order";
}
